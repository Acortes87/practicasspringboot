package com.makotojava.learn.springboot.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

//Dos opciones implementar la interfaz o extender la clase abstracta que ya trae los 3 métodos vacios
//public class AbstractServiceInterceptor implements HandlerInterceptor

public class AbstractServiceInterceptor extends HandlerInterceptorAdapter {

	private String name = this.getClass().getSimpleName();

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("Pre Handle method is Calling " + "<" + name + "> ");
		long tiempoInicio = System.currentTimeMillis();
		request.setAttribute("tiempoInicio", tiempoInicio);
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		long tiempoInicio = (Long) request.getAttribute("tiempoInicio");
		request.removeAttribute("tiempoInicio");
		long tiempoFin = System.currentTimeMillis();
		long tiempoTranscurrido = tiempoFin - tiempoInicio;
		System.out.println(
				"Post Handle method is Calling " + "<" + name + "> " + " [Tiempo Ejecucion]:" + tiempoTranscurrido);

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		System.out.println("Request and Response is completed " + "<" + name + "> ");

	}

}
