package com.makotojava.learn.springboot.web;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.makotojava.learn.odot.exception.ServiceException;
import com.makotojava.learn.odot.model.Category;
import com.makotojava.learn.springboot.SpringBootDemoController;

@RestController
@RequestMapping("/CategoryRestService")
public class CategoryRestService extends SpringBootDemoController {


  @GetMapping("/FindAll")
  public List<Category> findAll() {
    return getCategoryService().findAll();
  }

  @GetMapping("/FindById/{id}")
  public Category findById(@PathVariable Long id) {
    return getCategoryService().findById(id);
  }

  @GetMapping("/FindByName/{name}")
  public Category findByName(@PathVariable String name) {
    return getCategoryService().findByName(name);
  }

  @PutMapping("/Add")
  public Category add(@RequestBody Category category) {
    Category ret;
    try {
      ret = getCategoryService().add(category);
    } catch (ServiceException e) {
      throw new RuntimeException("Could not add Category: " + ReflectionToStringBuilder.toString(category), e);
    }
    return ret;
  }

  @PostMapping("/Update")
  public String update(@RequestBody Category category) {
    String ret = "UPDATE FAILED";
    try {
      boolean updated = getCategoryService().update(category);
      if (updated) {
        ret = "UPDATE SUCCESSFUL";
      }
    } catch (ServiceException e) {
      throw new RuntimeException("Could not update Category: " + ReflectionToStringBuilder.toString(category), e);
    }
    return ret;
  }

  @DeleteMapping("/Delete")
  public String delete(@RequestBody Category category) {
    String ret = "DELETE FAILED";
    try {
      getCategoryService().delete(category);
      ret = "DELETE SUCCESSFUL";
    } catch (ServiceException e) {
      throw new RuntimeException("Could not delete Category: " + ReflectionToStringBuilder.toString(category), e);
    }
    return ret;
  }

}
