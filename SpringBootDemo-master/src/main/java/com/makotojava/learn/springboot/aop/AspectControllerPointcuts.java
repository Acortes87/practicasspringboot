package com.makotojava.learn.springboot.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AspectControllerPointcuts {

	// la ejecución de cualquier método definido en el package
	// com.xyz.serviceo sub-package:
	@Pointcut("execution(* com.makotojava.learn.springboot.web..*.*(..))")
    public void allController(){}
	
	
	
	
}
