package com.makotojava.learn.springboot.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Component
public class InterceptorAppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	CategoryRestServiceInterceptor categoryRestServiceInterceptor;
	
	@Autowired
	ItemRestServiceInterceptor itemRestServiceInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		
		registry.addInterceptor(categoryRestServiceInterceptor).addPathPatterns("/CategoryRestService/*");
		registry.addInterceptor(itemRestServiceInterceptor).addPathPatterns("/ItemRestService/*");

	}

}
