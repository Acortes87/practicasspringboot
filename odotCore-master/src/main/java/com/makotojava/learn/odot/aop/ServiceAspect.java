package com.makotojava.learn.odot.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ServiceAspect {
	
	private static final Logger log = LoggerFactory.getLogger(ServiceAspect.class);

	@Around("AspectServicePointcuts.allServices()")
	public Object rendimientoServices(ProceedingJoinPoint joinPoint) throws Throwable {
		long t1 = System.currentTimeMillis();

		Object resultado = joinPoint.proceed();

		long t2 = System.currentTimeMillis();

		log.info(this.getClass().getSimpleName() +"-->"+joinPoint.getTarget().getClass() + "." + joinPoint.getSignature().getName() + "--> Tiempo:"
				+ "[" + (t2 - t1) + "]");
//		System.out.println(this.getClass().getSimpleName() +"-->"+joinPoint.getTarget().getClass() + "." + joinPoint.getSignature().getName() + "--> Tiempo:"
//				+ "[" + (t2 - t1) + "]");

		return resultado;

	}

}
