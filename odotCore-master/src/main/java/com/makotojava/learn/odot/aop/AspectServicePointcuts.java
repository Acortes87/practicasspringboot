package com.makotojava.learn.odot.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AspectServicePointcuts {

	// la ejecución de cualquier método definido en el package
	// com.xyz.serviceo sub-package:
	@Pointcut("execution(* com.makotojava.learn.odot.service..*.*(..))")
    public void allServices(){}
	
	
	
	
}
